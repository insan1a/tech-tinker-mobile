import 'package:flutter/material.dart';
import 'package:snippet_coder_utils/FormHelper.dart';
import 'package:snippet_coder_utils/ProgressHUD.dart';
import 'package:tech_tinker/constants/constants.dart';
import 'package:tech_tinker/models/login_request.dart';
import 'package:tech_tinker/services/api.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  bool isAPICallProcess = false;
  bool hidePassword = true;
  GlobalKey<FormState> globalFormKey = GlobalKey<FormState>();
  String? email;
  String? password;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        body: ProgressHUD(
          key: UniqueKey(),
          opacity: 0.3,
          inAsyncCall: isAPICallProcess,
          child: Form(
            child: _loginUI(context),
            key: globalFormKey,
          ),
        ),
      ),
    );
  }

  _loginUI(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height / 6,
            decoration: const BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  Colors.orange,
                  Colors.orange,
                ],
              ),
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(100),
                bottomRight: Radius.circular(100),
              ),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Align(
                  alignment: Alignment.center,
                  child: Image.asset(
                    "assets/images/TechTinkerAppLogo.png",
                    width: 300,
                    fit: BoxFit.contain,
                  ),
                ),
              ],
            ),
          ),
          const Padding(
            padding: EdgeInsets.only(
              left: 20,
              bottom: 10,
              top: 50,
            ),
          ),
          FormHelper.inputFieldWidget(
            context,
            "email",
            "Email",
            (onValidate) {
              if (onValidate.isEmpty) {
                return "Email can\'t be empty.";
              }
              return null;
            },
            (onSaved) {
              email = onSaved;
            },
            fontSize: 16,
            borderFocusColor: Colors.orange,
            borderColor: Colors.orange,
            hintColor: Colors.black.withOpacity(0.5),
            textColor: Colors.black,
            contentPadding: 20,
            borderRadius: 10,
          ),
          Padding(
            padding: const EdgeInsets.only(
              top: 10,
              bottom: 20,
            ),
            child: FormHelper.inputFieldWidget(
              context,
              "password",
              "Password",
              (onValidate) {
                if (onValidate.isEmpty) {
                  return "Password can't be empty.";
                }
                return null;
              },
              (onSaved) {
                password = onSaved;
              },
              fontSize: 16,
              borderFocusColor: Colors.orange,
              borderColor: Colors.orange,
              hintColor: Colors.black.withOpacity(0.5),
              textColor: Colors.black,
              contentPadding: 20,
              borderRadius: 10,
              obscureText: hidePassword,
              suffixIcon: IconButton(
                onPressed: () {
                  setState(() {
                    hidePassword = !hidePassword;
                  });
                },
                icon: Icon(
                  hidePassword ? Icons.visibility_off : Icons.visibility,
                ),
              ),
            ),
          ),
          Center(
            child: FormHelper.submitButton(
              "Log In",
              () {
                if (validateAndSave()) {
                  setState(() {
                    isAPICallProcess = true;
                  });
                }

                var model =
                    LoginRequestModel(email: email!, password: password!);

                APIService.login(model).then(
                  (response) {
                    setState(() {
                      isAPICallProcess = false;
                    });
                    if (response) {
                      Navigator.pushNamedAndRemoveUntil(
                        context,
                        "/home",
                        (route) => false,
                      );
                    } else {
                      FormHelper.showSimpleAlertDialog(
                        context,
                        appName,
                        "Invalid login credentials",
                        "OK",
                        () {
                          Navigator.pop(context);
                        },
                      );
                    }
                  },
                );
              },
              btnColor: Colors.orange,
              borderRadius: 10,
              width: MediaQuery.of(context).size.width / 1.1,
            ),
          ),
        ],
      ),
    );
  }

  bool validateAndSave() {
    final form = globalFormKey.currentState;
    if (form!.validate()) {
      form.save();
      return true;
    }
    return false;
  }
}
