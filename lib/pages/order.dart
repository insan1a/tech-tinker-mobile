import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tech_tinker/core/currency_formatter.dart';
import 'package:tech_tinker/models/configuration.dart';
import 'package:tech_tinker/models/order.dart';
import 'package:tech_tinker/services/orders.dart';
import 'package:tech_tinker/widgets/configuration_list.dart';
import 'package:tech_tinker/widgets/loader.dart';
import 'package:tech_tinker/widgets/order_status_badge.dart';

class OrderPage extends StatelessWidget {
  OrderPage({
    super.key,
    required this.order,
  });

  final OrderModel order;
  final formatter = currencyFormatter();

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: OrderService.getOrder(order.id),
      builder: (context, snapshot) {
        if (snapshot.hasData == false) {
          return const Loader();
        }

        var order = snapshot.data;
        var customer = order?.customer;
        var configs = order?.configurations;

        return Scaffold(
          appBar: AppBar(
            backgroundColor: CupertinoColors.systemOrange,
            leading: IconButton(
              icon: const Icon(
                CupertinoIcons.back,
                size: 30,
                color: CupertinoColors.white,
              ),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          ),
          body: SingleChildScrollView(
            padding: const EdgeInsets.all(30),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    "Order #${order?.number}",
                    style: const TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 28,
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    "Limit ${formatter.format(order!.priceLimit / 100)} RUB",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: CupertinoColors.black.withOpacity(0.7),
                      fontSize: 18,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 10),
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          "${customer?.firstName} ${customer?.lastName}",
                          style: const TextStyle(
                            fontWeight: FontWeight.bold,
                            color: CupertinoColors.systemOrange,
                            fontSize: 18,
                          ),
                        ),
                      ),
                      Align(
                        alignment: Alignment.centerRight,
                        child: OrderStatusBadge(
                          status: order.status,
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 10),
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      order.address,
                      style: const TextStyle(
                        fontSize: 18,
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 10),
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: SingleChildScrollView(
                      child: Text(
                        order.comment,
                        style: const TextStyle(
                          fontSize: 16,
                        ),
                      ),
                    ),
                  ),
                ),
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Align(
                      alignment: Alignment.centerLeft,
                      child: Padding(
                        padding: EdgeInsets.only(top: 10, bottom: 10),
                        child: Text(
                          "Configurations",
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 24,
                          ),
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.centerRight,
                      child: IconButton(
                        icon: const Icon(
                          CupertinoIcons.plus,
                          color: CupertinoColors.systemOrange,
                          size: 36,
                        ),
                        onPressed: () {},
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height,
                  child: buildConfigView(configs),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  Widget buildConfigView(List<ConfigurationModel>? configs) {
    if (configs == null) {
      return Container();
    }

    return ConfigurationList(configs: configs);
  }
}
