import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tech_tinker/pages/orders.dart';
import 'package:tech_tinker/pages/profile.dart';
import 'package:tech_tinker/pages/statistic.dart';
import 'package:tech_tinker/services/auth.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _selectedIndex = 0;
  static const List<Widget> _pages = <Widget>[
    OrdersPage(),
    StatisticPage(),
    ProfileScreen(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: CupertinoColors.systemOrange,
        actions: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15),
            child: IconButton(
              onPressed: () {
                AuthService.logout(context);
              },
              icon: const Icon(
                CupertinoIcons.xmark_octagon,
                color: CupertinoColors.white,
                size: 34,
              ),
            ),
          ),
        ],
      ),
      body: IndexedStack(
        index: _selectedIndex,
        children: _pages,
      ),
      bottomNavigationBar: BottomNavigationBar(
        selectedItemColor: CupertinoColors.activeOrange,
        selectedIconTheme: const IconThemeData(
          color: CupertinoColors.activeOrange,
          size: 40,
        ),
        selectedLabelStyle: const TextStyle(
          fontWeight: FontWeight.bold,
        ),
        selectedFontSize: 16,
        iconSize: 36,
        items: const [
          BottomNavigationBarItem(
            label: "Orders",
            icon: Icon(CupertinoIcons.news),
          ),
          BottomNavigationBarItem(
            label: "Statistic",
            icon: Icon(CupertinoIcons.chart_pie),
          ),
          BottomNavigationBarItem(
            label: "Profile",
            icon: Icon(CupertinoIcons.person),
          ),
        ],
        currentIndex: _selectedIndex,
        onTap: _onItemTapped,
      ),
    );
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }
}
