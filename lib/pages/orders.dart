import 'package:flutter/material.dart';
import 'package:tech_tinker/services/orders.dart';
import 'package:tech_tinker/widgets/loader.dart';
import 'package:tech_tinker/widgets/order_list.dart';

class OrdersPage extends StatefulWidget {
  const OrdersPage({super.key});

  @override
  State<OrdersPage> createState() => _OrdersPageState();
}

class _OrdersPageState extends State<OrdersPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: ordersPage(),
    );
  }

  Widget ordersPage() {
    return FutureBuilder(
      future: OrderService.getOrders(),
      builder: (context, models) {
        if (models.hasData == false) {
          return const Loader();
        }

        return OrdersView(orders: models.data!);
      },
    );
  }
}
