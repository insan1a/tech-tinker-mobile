import 'package:flutter/cupertino.dart';
import 'package:pie_chart/pie_chart.dart';
import 'package:tech_tinker/services/stats.dart';

class StatisticPage extends StatelessWidget {
  const StatisticPage({super.key});

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: StatsService.getStatistic(DateTime.now(), DateTime.now()),
      builder: (context, snapshot) {
        // if (snapshot.data == null) {
        //   return const Loader();
        // }

        var map = <String, double>{
          "test": 2.0,
          "test2": 2.0,
          "test3": 2.0,
          "test4": 2.0,
        };

        return Center(
          child: PieChart(
            colorList: const [
              CupertinoColors.systemBlue,
              CupertinoColors.systemGreen,
              CupertinoColors.systemRed,
              CupertinoColors.systemYellow,
            ],
            dataMap: map,
            chartRadius: MediaQuery.of(context).size.width / 2,
            chartType: ChartType.ring,
            legendOptions: const LegendOptions(
              showLegendsInRow: true,
              legendPosition: LegendPosition.bottom,
            ),
          ),
        );
      },
    );
  }
}
