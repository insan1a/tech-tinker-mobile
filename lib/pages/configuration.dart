import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:snippet_coder_utils/FormHelper.dart';
import 'package:tech_tinker/core/currency_formatter.dart';
import 'package:tech_tinker/models/configuration.dart';
import 'package:tech_tinker/services/api.dart';
import 'package:tech_tinker/widgets/component_list.dart';
import 'package:tech_tinker/widgets/loader.dart';

class ConfigurationPage extends StatelessWidget {
  ConfigurationPage({
    super.key,
    required this.config,
  });

  final ConfigurationModel config;
  final formatter = currencyFormatter();

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: APIService.getConfiguration(config.id),
      builder: (context, snapshot) {
        if (snapshot.data == null) {
          return const Loader();
        }

        var config = snapshot.data;

        return Scaffold(
          appBar: AppBar(
            backgroundColor: CupertinoColors.systemOrange,
            leading: IconButton(
              icon: const Icon(
                CupertinoIcons.back,
                size: 30,
                color: CupertinoColors.white,
              ),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          ),
          body: SingleChildScrollView(
            padding: const EdgeInsets.all(30),
            child: Column(
              children: [
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "Configuration",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 24,
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.centerRight,
                      child: Text(
                        "${formatter.format(config!.price / 100)} RUB",
                        style: const TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 18,
                          color: CupertinoColors.systemOrange,
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height / 1.55,
                  child: buildComponentView(config.components),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 10),
                  child: FormHelper.submitButton(
                    "Change",
                    () {},
                    btnColor: CupertinoColors.systemOrange,
                    fontWeight: FontWeight.bold,
                    fontSize: 18,
                    width: MediaQuery.of(context).size.width,
                    height: 50,
                    borderRadius: 10,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 10),
                  child: FormHelper.submitButton(
                    "Delete",
                    () {},
                    btnColor: CupertinoColors.systemRed,
                    fontWeight: FontWeight.bold,
                    fontSize: 18,
                    width: MediaQuery.of(context).size.width,
                    height: 50,
                    borderRadius: 10,
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  Widget buildComponentView(List<Component>? components) {
    if (components == null) {
      return Container();
    }

    return ComponentList(components: components);
  }
}
