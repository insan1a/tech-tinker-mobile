import 'package:http/http.dart' as http;
import 'package:tech_tinker/constants/constants.dart';
import 'package:tech_tinker/models/user.dart';
import 'package:tech_tinker/services/auth.dart';

class ProfileService {
  static var client = http.Client();

  static Future<UserModel?> getUserProfile() async {
    var loginDetails = await AuthService.loginDetails();
    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${loginDetails!.token}'
    };
    var url = Uri.http(baseUrl, profileEndpoint);

    var response = await client.get(
      url,
      headers: headers,
    );

    if (response.statusCode == 200) {
      var user = UserModel.fromRawJson(response.body);
      return user;
    }

    return null;
  }
}
