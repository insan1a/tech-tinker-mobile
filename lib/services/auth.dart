import 'package:api_cache_manager/api_cache_manager.dart';
import 'package:api_cache_manager/models/cache_db_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:tech_tinker/models/login_response.dart';

class AuthService {
  static String loginDetailsKey = "login_details";

  static Future<bool> isLoggedIn() async {
    var isKeyExists =
        await APICacheManager().isAPICacheKeyExist(loginDetailsKey);

    return isKeyExists;
  }

  static Future<LoginResponseModel?> loginDetails() async {
    var isKeyExists =
        await APICacheManager().isAPICacheKeyExist(loginDetailsKey);

    if (isKeyExists) {
      var cacheData = await APICacheManager().getCacheData(loginDetailsKey);

      return LoginResponseModel.fromRawJson(cacheData.syncData);
    }

    return null;
  }

  static Future<void> setLoginDetails(LoginResponseModel model) async {
    APICacheDBModel cacheDBModel = APICacheDBModel(
      key: loginDetailsKey,
      syncData: model.toRawJson(),
    );

    await APICacheManager().addCacheData(cacheDBModel);
  }

  static Future<void> logout(context) async {
    await APICacheManager().deleteCache(loginDetailsKey);
    Navigator.pushNamedAndRemoveUntil(
      context,
      "/login",
      (route) => false,
    );
  }
}
