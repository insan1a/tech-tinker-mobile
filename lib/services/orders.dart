import 'package:tech_tinker/models/order.dart';
import 'package:tech_tinker/services/api.dart';
import 'package:tech_tinker/services/auth.dart';

class OrderService {
  static Future<List<OrderModel>?> getOrders() async {
    var loginDetails = await AuthService.loginDetails();
    return await APIService.getOrders(loginDetails!.token);
  }

  static Future<OrderModel?> getOrder(String id) async {
    var loginDetails = await AuthService.loginDetails();
    return await APIService.getOrder(id, loginDetails!.token);
  }
}
