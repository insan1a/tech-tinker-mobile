import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:tech_tinker/constants/constants.dart';
import 'package:tech_tinker/models/configuration.dart';
import 'package:tech_tinker/models/customer.dart';
import 'package:tech_tinker/models/login_request.dart';
import 'package:tech_tinker/models/login_response.dart';
import 'package:tech_tinker/models/order.dart';
import 'package:tech_tinker/models/statistic.dart';
import 'package:tech_tinker/services/auth.dart';

class APIService {
  static var client = http.Client();

  static Future<bool> login(LoginRequestModel model) async {
    var url = Uri.http(baseUrl, authEndpoint);
    Map<String, String> headers = {
      'Content-Type': 'application/json',
    };

    var response = await client.post(url,
        headers: headers, body: jsonEncode(model.toJson()));

    if (response.statusCode == 200) {
      await AuthService.setLoginDetails(
          LoginResponseModel.fromRawJson(response.body));

      return true;
    }

    return false;
  }

  static Future<List<OrderModel>?> getOrders(String token) async {
    var url = Uri.http(baseUrl, ordersEndpoint);
    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer $token',
    };

    var response = await client.get(url, headers: headers);

    if (response.statusCode == 200) {
      List<dynamic> jsonList = json.decode(response.body);
      List<OrderModel> orders =
          jsonList.map((e) => OrderModel.fromJson(e)).toList();
      return orders;
    }

    return null;
  }

  static Future<OrderModel?> getOrder(String id, String token) async {
    var orderEndpoint = "$ordersEndpoint/$id";
    var url = Uri.http(baseUrl, orderEndpoint);
    var headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer $token',
    };

    var response = await client.get(url, headers: headers);

    if (response.statusCode == 200) {
      var jsonData = json.decode(response.body);

      var order = OrderModel.fromJson(jsonData);

      List<dynamic> jsonList = jsonData['configurations'];

      order.customer = CustomerModel.fromJson(jsonData['customer']);
      order.configurations = List<ConfigurationModel>.from(
          jsonList.map((e) => ConfigurationModel.fromJson(e)));

      return order;
    }

    return null;
  }

  static Future<Stats?> getStatistic(DateTime from, DateTime to) async {
    var loginDetails = await AuthService.loginDetails();
    var url = Uri.http(
      baseUrl,
      statsEndpoint,
      {
        "from": from,
        "to": to,
      },
    );
    var headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${loginDetails!.token}'
    };

    var response = await client.get(url, headers: headers);

    if (response.statusCode == 200) {
      return Stats.fromRawJson(response.body);
    }

    return null;
  }

  static Future<ConfigurationModel?> getConfiguration(String id) async {
    var loginDetails = await AuthService.loginDetails();
    var url = Uri.http(
      baseUrl,
      "$configsEndpoint/$id",
    );
    var headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${loginDetails!.token}'
    };

    var response = await client.get(url, headers: headers);

    if (response.statusCode == 200) {
      var jsonData = json.decode(response.body);

      var config = ConfigurationModel.fromJson(jsonData);

      List<dynamic>? jsonList = jsonData['components'];
      if (jsonList != null) {
        config.components =
            List<Component>.from(jsonList.map((e) => Component.fromJson(e)));
      }

      return config;
    }

    return null;
  }
}
