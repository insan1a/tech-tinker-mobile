import 'package:tech_tinker/models/statistic.dart';
import 'package:tech_tinker/services/api.dart';

class StatsService {
  static Future<Stats?> getStatistic(DateTime from, DateTime to) async {
    return APIService.getStatistic(from, to);
  }
}
