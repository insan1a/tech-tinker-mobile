import 'dart:convert';

class CustomerModel {
  final String? id;
  final String? firstName;
  final String? lastName;
  final String? email;
  final String? phone;

  CustomerModel({
    this.id,
    this.firstName,
    this.lastName,
    this.email,
    this.phone,
  });

  factory CustomerModel.fromRawJson(String str) =>
      CustomerModel.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory CustomerModel.fromJson(Map<String, dynamic> json) {
    return CustomerModel(
      id: json["id"],
      firstName: json["first_name"],
      lastName: json["last_name"],
      email: json["email"],
      phone: json["phone"],
    );
  }

  Map<String, dynamic> toJson() => {
        "id": id,
        "first_name": firstName,
        "last_name": lastName,
        "email": email,
        "phone": phone,
      };
}
