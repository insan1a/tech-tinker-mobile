import 'dart:convert';

import 'package:tech_tinker/models/customer.dart';

import 'configuration.dart';

class OrderModel {
  final String id;
  final int number;
  CustomerModel? customer;
  final int priceLimit;
  final String comment;
  final String address;
  final String status;
  final DateTime createdAt;
  List<ConfigurationModel>? configurations;

  OrderModel({
    required this.id,
    required this.number,
    this.customer,
    required this.priceLimit,
    required this.comment,
    required this.address,
    required this.status,
    required this.createdAt,
    this.configurations,
  });

  factory OrderModel.fromRawJson(String str) =>
      OrderModel.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory OrderModel.fromJson(Map<String, dynamic> json) {
    return OrderModel(
      id: json["id"],
      number: json["number"],
      priceLimit: json["price_limit"],
      comment: json["comment"],
      address: json["address"],
      status: json["status"],
      createdAt: DateTime.parse(json["created_at"]),
    );
  }

  Map<String, dynamic> toJson() => {
        "id": id,
        "number": number,
        "customer": customer!.toJson(),
        "price_limit": priceLimit,
        "comment": comment,
        "address": address,
        "status": status,
        "created_at": createdAt.toIso8601String(),
        "configurations":
            List<dynamic>.from(configurations!.map((x) => x.toJson())),
      };
}
