import 'dart:convert';

class ConfigurationModel {
  final String id;
  final int price;
  final DateTime createdAt;
  List<Component>? components;

  ConfigurationModel({
    required this.id,
    required this.price,
    required this.createdAt,
    this.components,
  });

  factory ConfigurationModel.fromRawJson(String str) =>
      ConfigurationModel.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory ConfigurationModel.fromJson(Map<String, dynamic> json) =>
      ConfigurationModel(
        id: json["id"],
        price: json["price"],
        createdAt: DateTime.parse(json["created_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "price": price,
        "created_at": createdAt.toIso8601String(),
        "components": List<dynamic>.from(components!.map((x) => x.toJson())),
      };
}

class Component {
  final String id;
  final String name;
  final String description;
  final String type;
  final int price;

  Component({
    required this.id,
    required this.name,
    required this.description,
    required this.type,
    required this.price,
  });

  factory Component.fromRawJson(String str) =>
      Component.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Component.fromJson(Map<String, dynamic> json) => Component(
        id: json["id"],
        name: json["name"],
        description: json["description"],
        type: json["type"],
        price: json['price'],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "description": description,
        "type": type,
        "price": price,
      };
}
