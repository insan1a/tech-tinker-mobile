import 'dart:convert';

class Stats {
  final Period period;
  final int total;
  final List<Budget> budgets;

  Stats({
    required this.period,
    required this.total,
    required this.budgets,
  });

  factory Stats.fromRawJson(String str) => Stats.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Stats.fromJson(Map<String, dynamic> json) => Stats(
        period: Period.fromJson(json["period"]),
        total: json["total"],
        budgets:
            List<Budget>.from(json["budgets"].map((x) => Budget.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "period": period.toJson(),
        "total": total,
        "budgets": List<dynamic>.from(budgets.map((x) => x.toJson())),
      };
}

class Budget {
  final String type;
  final int count;

  Budget({
    required this.type,
    required this.count,
  });

  factory Budget.fromRawJson(String str) => Budget.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Budget.fromJson(Map<String, dynamic> json) => Budget(
        type: json["type"],
        count: json["count"],
      );

  Map<String, dynamic> toJson() => {
        "type": type,
        "count": count,
      };
}

class Period {
  final DateTime start;
  final DateTime end;

  Period({
    required this.start,
    required this.end,
  });

  factory Period.fromRawJson(String str) => Period.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Period.fromJson(Map<String, dynamic> json) => Period(
        start: DateTime.parse(json["start"]),
        end: DateTime.parse(json["end"]),
      );

  Map<String, dynamic> toJson() => {
        "start": start.toIso8601String(),
        "end": end.toIso8601String(),
      };
}
