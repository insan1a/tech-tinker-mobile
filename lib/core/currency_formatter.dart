import 'package:intl/intl.dart';

NumberFormat currencyFormatter() => NumberFormat("#,##0.00", "en_US");
