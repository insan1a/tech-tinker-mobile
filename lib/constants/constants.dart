const String baseUrl = "10.0.2.2:5124";
const String profileEndpoint = "/api/current";
const String ordersEndpoint = "/api/current/orders";
const String statsEndpoint = "/api/current/statistic";
const String authEndpoint = "/api/auth/token";
const String appName = "Tech Tinker";
const String configsEndpoint = "/api/configs";
