import 'package:flutter/material.dart';
import 'package:tech_tinker/pages/home.dart';
import 'package:tech_tinker/pages/login.dart';
import 'package:tech_tinker/services/auth.dart';

Widget _defaultHome = const LoginPage();

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  bool result = await AuthService.isLoggedIn();
  if (result) {
    _defaultHome = const HomePage();
  }

  runApp(const TechTinkerApp());
}

class TechTinkerApp extends StatelessWidget {
  const TechTinkerApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Tech Tinker',
      theme: ThemeData(
        primarySwatch: Colors.orange,
        useMaterial3: true,
      ),
      routes: {
        "/": (context) => _defaultHome,
        "/home": (context) => const HomePage(),
        "/login": (context) => const LoginPage(),
      },
    );
  }
}
