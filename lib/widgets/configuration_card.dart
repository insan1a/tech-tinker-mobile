import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tech_tinker/core/currency_formatter.dart';
import 'package:tech_tinker/models/configuration.dart';
import 'package:tech_tinker/pages/configuration.dart';

class ConfigurationCard extends StatelessWidget {
  ConfigurationCard({
    super.key,
    required this.config,
  });

  final ConfigurationModel config;
  final formatter = currencyFormatter();

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Card(
        color: CupertinoColors.lightBackgroundGray,
        child: Column(
          children: [
            ListTile(
              title: Text(
                "Price: ${formatter.format(config.price / 100)} RUB",
              ),
              subtitle: Text(
                "Created at ${config.createdAt}",
              ),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => ConfigurationPage(config: config),
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
