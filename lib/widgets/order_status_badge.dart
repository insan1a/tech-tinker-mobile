import 'package:badges/badges.dart' as badges;
import 'package:flutter/cupertino.dart';

class OrderStatusBadge extends StatelessWidget {
  const OrderStatusBadge({super.key, required this.status});

  final String status;

  @override
  Widget build(BuildContext context) {
    return badges.Badge(
      badgeContent: Text(
        status,
        style: const TextStyle(
          fontWeight: FontWeight.w600,
          fontSize: 12,
          color: CupertinoColors.white,
        ),
      ),
      badgeStyle: badges.BadgeStyle(
        shape: badges.BadgeShape.square,
        badgeColor: getBadgeColor(status),
        borderRadius: BorderRadius.circular(100),
        padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
      ),
    );
  }

  CupertinoDynamicColor getBadgeColor(String status) {
    switch (status) {
      case "InProcess":
        return CupertinoColors.activeBlue;
      case "Completed":
        return CupertinoColors.systemRed;
      default:
        return CupertinoColors.activeBlue;
    }
  }
}
