import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tech_tinker/core/currency_formatter.dart';
import 'package:tech_tinker/models/configuration.dart';

class ComponentCard extends StatelessWidget {
  ComponentCard({
    super.key,
    required this.component,
  });

  final Component component;
  final formatter = currencyFormatter();

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Card(
        color: CupertinoColors.lightBackgroundGray,
        child: Column(
          children: [
            ListTile(
              title: Text(
                component.name,
                style: const TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 18,
                ),
              ),
              subtitle: Text(
                "${formatter.format(component.price / 100)} RUB",
                style: const TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16,
                  color: CupertinoColors.systemOrange,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
