import 'package:flutter/material.dart';
import 'package:tech_tinker/models/order.dart';
import 'package:tech_tinker/widgets/order_card.dart';

class OrdersView extends StatelessWidget {
  const OrdersView({
    super.key,
    required this.orders,
  });

  final List<OrderModel> orders;

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      itemCount: orders.length,
      itemBuilder: cardBuilder,
    );
  }

  Widget cardBuilder(BuildContext context, int index) {
    return OrderCard(
      order: orders[index],
    );
  }
}
