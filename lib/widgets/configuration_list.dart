import 'package:flutter/material.dart';
import 'package:tech_tinker/models/configuration.dart';
import 'package:tech_tinker/widgets/configuration_card.dart';

class ConfigurationList extends StatelessWidget {
  const ConfigurationList({super.key, required this.configs});

  final List<ConfigurationModel> configs;

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: configs.length,
      itemBuilder: (context, index) {
        return ConfigurationCard(config: configs[index]);
      },
    );
  }
}
