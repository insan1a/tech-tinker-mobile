import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:snippet_coder_utils/FormHelper.dart';
import 'package:tech_tinker/core/currency_formatter.dart';
import 'package:tech_tinker/models/order.dart';
import 'package:tech_tinker/pages/order.dart';
import 'package:tech_tinker/widgets/order_status_badge.dart';

class OrderCard extends StatelessWidget {
  OrderCard({
    super.key,
    required this.order,
  });

  final OrderModel order;
  final formatter = currencyFormatter();

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Card(
        color: CupertinoColors.white,
        child: Column(
          children: [
            ListTile(
              title: Text(
                "Order ${order.number}",
                style: const TextStyle(
                  fontWeight: FontWeight.bold,
                  color: CupertinoColors.activeOrange,
                  fontSize: 16,
                ),
              ),
              subtitle: Text(
                "Limit ${formatter.format(order.priceLimit / 100)} RUB",
                style: const TextStyle(
                  color: CupertinoColors.black,
                ),
              ),
              trailing: OrderStatusBadge(status: order.status),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 10, left: 10, right: 10),
              child: FormHelper.submitButton(
                "View",
                () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => OrderPage(order: order),
                    ),
                  );
                },
                btnColor: CupertinoColors.systemOrange,
                fontWeight: FontWeight.bold,
                fontSize: 18,
                borderRadius: 10,
                height: 50,
                width: MediaQuery.of(context).size.width,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
