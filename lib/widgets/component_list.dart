import 'package:flutter/material.dart';
import 'package:tech_tinker/models/configuration.dart';
import 'package:tech_tinker/widgets/component_card.dart';

class ComponentList extends StatelessWidget {
  const ComponentList({super.key, required this.components});

  final List<Component> components;

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: components.length,
      itemBuilder: (context, index) {
        return ComponentCard(component: components[index]);
      },
    );
  }
}
